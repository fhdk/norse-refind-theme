# rEFInd theme Norse

A simplistic clean and minimal theme for rEFInd


### Installation:

1. Clone git repository to your $HOME directory.
   ```
   git clone https://codeberg.org/fhdk/norse-refind-theme.git
   ```
2. Locate refind directory under EFI partition. For most Linux based system is commonly `/boot/efi/EFI/refind/`. Copy theme directory to it.

   **Important:** Delete older installed versions of this theme before you proceed any further.

   ```
   sudo rm -rf /boot/efi/EFI/refind/norse-refind-theme
   ```
   ```
   sudo cp -r norse-refind-theme /boot/efi/EFI/refind/
   ```
3. To enable the theme add `include norse-refind-theme/theme.conf` at the end of `refind.conf`.
   ```
   sudo nano /boot/efi/EFI/refind/refind.conf
   ```

**More information**

[rEFInd](http://www.rodsbooks.com/refind/) An official rEFInd website
